<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ page import="database.area.*" %>
<%
/* 这是学生信息表的表结构
       CREATE TABLE `t_student` (
         `id` VARCHAR(50) COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '学生编号',
         `loginname` VARCHAR(50) COLLATE utf8_general_ci DEFAULT NULL COMMENT '帐号',
         `name` VARCHAR(50) COLLATE utf8_general_ci DEFAULT NULL COMMENT '姓名',
         `age` INTEGER(11) DEFAULT NULL COMMENT '年龄(number)',
         `birthday` DATETIME DEFAULT NULL COMMENT '生日(datetime)',
         `gender` INTEGER(11) DEFAULT NULL COMMENT '性别：1男；2女',
         `country` VARCHAR(20) COLLATE utf8_general_ci DEFAULT NULL COMMENT '国家',
         `city` VARCHAR(20) COLLATE utf8_general_ci DEFAULT NULL COMMENT '城市',
         `remarks` VARCHAR(2000) COLLATE utf8_general_ci DEFAULT NULL COMMENT '备注',
         `school` VARCHAR(20) COLLATE utf8_general_ci DEFAULT NULL COMMENT '毕业学校',
         `createtime` DATETIME DEFAULT NULL,
         `email` VARCHAR(100) COLLATE utf8_general_ci DEFAULT NULL
       )ENGINE=InnoDB
       CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
       COMMENT='学生信息';
*/
    /*=============此JSP文件就是设置模板运行的参数,一般包括表字段信息,主键信息=========> */
    Table table = new Table();
    AppArea.table = table;
    table.name = "t_student";
    table.primaryKey="id"; // 指定此表主键

    /* 列信息 : 这些列信息不必完全靠手工编写,可以使用
    	test.DBTableParamlyzeTool.java 直接输出到控制台,然后手工粘贴到此即可.
       =>{
    */
    table.cols.add(new Column("学生编号","id","学生编号","varchar(50)","FALSE","TRUE"));
    table.cols.add(new Column("姓名","name","姓名","varchar(50)","FALSE","FALSE"));
    table.cols.add(new Column("年龄(number)","age","年龄(number)","int(11)","FALSE","FALSE"));
    table.cols.add(new Column("生日(datetime)","birthday","生日(datetime)","datetime","FALSE","FALSE"));
    table.cols.add(new Column("性别：1男；2女","gender","性别：1男；2女","int(11)","FALSE","FALSE"));
    table.cols.add(new Column("国家","country","国家","varchar(20)","FALSE","FALSE"));
    table.cols.add(new Column("城市","city","城市","varchar(20)","FALSE","FALSE"));
    table.cols.add(new Column("备注","remarks","备注","varchar(2000)","FALSE","FALSE"));
    table.cols.add(new Column("毕业学校","school","毕业学校","varchar(20)","FALSE","FALSE"));
    table.cols.add(new Column("","createtime","","datetime","FALSE","FALSE"));
    table.cols.add(new Column("","email","","varchar(100)","FALSE","FALSE"));
    /*  }*/
    /* <============================================================================= */

%>
