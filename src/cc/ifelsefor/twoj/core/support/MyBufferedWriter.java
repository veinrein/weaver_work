package cc.ifelsefor.twoj.core.support;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;


public class MyBufferedWriter{
	
	BufferedWriter out;
	
	boolean canStart;
	
	String[][] reps;
	
	public MyBufferedWriter(){
		
	}
	
	public MyBufferedWriter(File outfile, String pageEncoding, String[][] reps) {
		// TODO Auto-generated constructor stub
		this.setOut(outfile, pageEncoding, reps);
	}

	public boolean isCanStart() {
		return canStart;
	}

	public void setCanStart(boolean canStart) {
		this.canStart = canStart;
	}

	public String[][] getReps() {
		return reps;
	}

	public void setReps(String[][] reps) {
		this.reps = reps;
	}

	/**
	 * WRITE方法:20140918
	 */
	public void write(String s) throws IOException {
		if(this.out!=null){
			// 检测分割线，必须从分割线以下才开始写
			for(int i=0; i<this.reps.length; i++){
				s = s.replace(reps[i][1], reps[i][0]);
			}
			this.out.write(s);
			// System.out.println("内容输出"+canStart+":"+s);
		}
	}
	
	public void flush() throws IOException{
		this.out.flush();
	}
	
	public void newLine() throws IOException{
		this.out.newLine();
	}
	
	public void setOut(File outfile,String pageEncoding,String[][] reps) {
	        setReps(reps);
	        try
	        {	
	            outfile.getParentFile().mkdirs();
	            FileOutputStream fos = new FileOutputStream(outfile);
	            // 强制BOM输出 
	            // if("UTF-8".equals(pageEncoding))
                // fos.write(new byte[] {
                //     -17, -69, -65
                //  });
	            this.out = new BufferedWriter(new OutputStreamWriter(fos, pageEncoding));
	        }
	        catch(IOException e)
	        {
	            e.printStackTrace();
	        }
	}
	
	public void close(){
		try {
			this.out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
